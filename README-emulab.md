moby (Docker) for Emulab
------------------------

We use a modified version of moby (docker) that supports isolated layer2
bridge networks; see the commits on the ```emulab``` branch.

Building
--------

You can either build the dockerd binary directly from source for quick
hacking, or you can build the `docker-ce` package
(https://github.com/docker/docker-ce) for your distro using our source
code.

Building from source:

```
export CGO_FLAGS=-I/usr/include
export CGO_LDFLAGS=-L/usr/lib64
export DOCKER_BUILDTAGS="exclude_graphdriver_aufs exclude_graphdriver_btrfs"
export AUTO_GOPATH=1
./hack/make.sh binary
ls -l bundles/binary-daemon/dockerd-dev
```

Packaging from source (requires `docker` (i.e. `docker-ce` or
`docker.io` on Ubuntu) to be installed):

```
export BUILDDIR=/path/to/build/dir
cd $BUILDDIR
git clone https://github.com/docker/docker-ce
git clone https://gitlab.flux.utah.edu/emulab/moby.git
cd docker-ce/components/packaging/deb
make ENGINE_DIR=$BUILDDIR/moby CLI_DIR=$BUILDDIR/moby ubuntu-xenial
ls -l debbuild/ubuntu-xenial/
```
